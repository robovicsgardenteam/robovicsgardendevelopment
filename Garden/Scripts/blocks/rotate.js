Blockly.Blocks['rotate'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Rotate with angle ');
	this.appendValueInput('A');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to rotate with specified angle');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};