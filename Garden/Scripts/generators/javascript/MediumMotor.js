Blockly.JavaScript['GripOpen'] = function(block) {
  code ='GripOpen();';
  return code;
};

Blockly.JavaScript['GripClose'] = function(block) {
  code ='GripClose();';
  return code;
};

Blockly.JavaScript['hammerUP'] = function(block) {
  code ='hammerUP();';
  return code;
};

Blockly.JavaScript['hammerdown'] = function(block) {
  code ='hammerdown();';
  return code;
};

Blockly.JavaScript['throwBall'] = function(block) {
  code ='throwBall();';
  return code;
};

Blockly.JavaScript['rotate_fan'] = function(block) {
 // Search the text for a substring.
var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';

	  var code =  'rotateFan('+ argument0+');';
  return code;
};
Blockly.JavaScript['rotate_fan_to'] = function(block) {
  // Search the text for a substring.
var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';

	  var code =  'rotateFanToAngle('+ argument0+');';
  return code;
};