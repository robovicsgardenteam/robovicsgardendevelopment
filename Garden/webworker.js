var ip = "localhost";

onmessage = function (e) {
    var data = e.data;
    if(ValidateIPaddress(data)){
        ip = data;
        var response = send("hi");
        postMessage("connect;connect Succecfully");
    }else{
        try {
            eval(data);
        } catch (e) {
            postMessage("error;"+e.message);
        }
    }
}

function ValidateIPaddress(inputText)
{
    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if(inputText.match(ipformat))
        return true;
    else
        return false;
}

function getDistanceIR() {
    return send(2);
}

function movForward() {
    return send('b c 1');
}

function rotateCCW() {
    return send('b d 0');
}

function rotateCW() {
    return send('b d 1');
}
function movBackward() {
    return send('b c 0');
}
function movArcForward(Radius) {
    return send('b b ' + Radius + ' 1');
}
function movArc(Radius, Angle) {
    return send('b a ' + Radius + ' ' + Angle);
}
function movArcBackward(Radius) {
    return send('b b ' + Radius + ' ' + '0');
}
function GripOpen() {
    return send('i B 2 -1395');
}
function GripClose() {
    return send('i B 2 1395');
}
function hammerUP() {
    return send('i B 2 1395');
}
function hammerdown() {
    return send('i B 2 -1395');
}
function throwBall() {
    send('i B 2 300');
    return send('i B 2 1095');
}
function rotateFan(angle) {
    return send('i B 2 ' + angle);
}
function rotateFanToAngle(angle) {
    return send('i B 1 ' + angle);
}
function stop() {
    return send('b g');
}
function travel(Distance) {
    return send('b e ' + Distance);
}
function rotate(Angle) {
    return send('b l ' + Angle);
}
function getColor() {
    var y = send(3);
    if(y == 7){
        return "Black";
    }else if(y == 2){
        return "Blue";
    }else if(y == 1){
        return "Green";
    }else if(y == 3){
        return "Yellow";
    }else if(y == 0){
        return "Red";
    }else if(y == 6){
        return "White";
    }else if(y == 13){
        return "Brown";
    }else{
        return "None";
    }
}

function print(text){
    postMessage("text;"+text);
}

function getSoundDb() {
    return send(8);
}
function getLIntensity() {
    return send(6);
}
function getTouchLevel() {
    return send(7);
}
function getDistanceUltraSonic() {
    return send(5);
}
function getRemoteCommand(channel) {
    var _channel = channel - 1;
    var val = send('1 ' + _channel);
    switch (val) {
        case 0 :
            return 'None';

        case 1 :
            return 'upLeft';

        case 2 :
            return 'downLeft';

        case 3 :
            return 'upRight';

        case 4 :
            return 'downRight';

        case 5 :
            return 'twoUpButtons';

        case 7 :
            return 'upRightDownLeft';

        case 8 :
            return 'twoDownButtons';

        case 6 :
            return 'upLeftDownRight';

        case 9 :
            return 'UP';

        default :
            return 'None';
    }
}


function send(id) {
    try{
        var response = get(id);
    }catch(err){
        postMessage("error;"+err.message);
        postMessage("error;Connection problem please check you enter ip correctly, and be sure that Emulator is running");
    }

    if(response.status != 200){
        postMessage("error;"+response.status);
    }else{
        return response.text;
    }
    return;
}

function get(id){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://"+ String(ip) +":8080/?id=" + id, false);
    xhr.send();
    var response = {
        status : xhr.status,
        text : xhr.responseText
    }
    //print("http://"+ String(ip) +":8080/?id=" + id + ",status=" + response.status);
    return response;
}