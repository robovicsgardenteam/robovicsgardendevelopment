<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }
}
