Blockly.Blocks['getColor_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(248);
	
	this.appendDummyInput()
	.appendField('Capture Cell Color');
    this.setOutput(true, 'String');
	this.setTooltip('enables you to capture the color in front of you');
  }
};