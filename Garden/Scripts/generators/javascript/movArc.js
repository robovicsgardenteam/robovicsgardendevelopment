Blockly.JavaScript['movArc'] = function(block) {
  // Search the text for a substring.
var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
var argument1 = Blockly.JavaScript.valueToCode(block, 'A',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
	  var code =  'movArc('+ argument0+','+argument1+');';
  return code;
};