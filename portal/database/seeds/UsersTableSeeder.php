<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname' => 'Ahmed',
            'lname' => 'Elsayed',
            'email' => 'admin@robovics.com',
            'password' => bcrypt('admin'),
            'image_name' => 'admin.png',
            'expired_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'limit' => null,
            'qouta' => null,
            'type_id' => 1,
            'status_id' => 1,
            'parent_id' => null,
        ]);
    }
}
