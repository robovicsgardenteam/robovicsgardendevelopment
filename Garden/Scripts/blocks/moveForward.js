Blockly.Blocks['moveForward'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(212);
	this.appendDummyInput()
	.appendField('Go Forward');
    this.setTooltip('enables the robot to move forward forever ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};