Blockly.Blocks['movArcBackward'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(210);
	this.appendDummyInput()
		.appendField('Move In Arc Backward with radius ');
	this.appendValueInput('R');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to rotate move in arc ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};