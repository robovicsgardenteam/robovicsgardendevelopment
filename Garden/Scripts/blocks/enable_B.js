Blockly.Blocks['enable_general'] = {
  init: function() {
  var PORTS =
         [['1', '1'],
         ['2','2'],
         ['3','3'],
         ['4','4']];
 var Peri =
         [['ColorSensor', '0'],
         ['IRSensor','1'],
		 ['Motor','2'],
		 ['UltraSonicSensor','3'],
		 ['TouchSensor','4'],
		 ['LightSensor','5'],
		 ['MediumMotor','6'],
		 ['SoundSensor','7'],
		 ['Fan','8'],
         ['Hammer','9'],
		  ['Gripper','10'],
         ['BallThrower','11']];
    this.setHelpUrl(null);
    this.setColour(0);
	
	this.appendDummyInput()
	.appendField('Enable')
	.appendField('Item')
    .appendField(new Blockly.FieldDropdown(Peri), 'pe')
	.appendField('On Port')
    .appendField(new Blockly.FieldDropdown(PORTS), 'po');
	this.setTooltip('enables you to capture the remote button ');
	this.setPreviousStatement(true);
	  this.setNextStatement(true);
  }
};



Blockly.Blocks['enable_pilot'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(0);
	this.appendDummyInput()
	.appendField('enable Pilot');
    this.setTooltip('enable The pilot');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};