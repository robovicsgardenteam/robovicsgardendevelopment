Blockly.Blocks['moveBackward'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(210);
	this.appendDummyInput()
	.appendField('Go Backward');
    this.setTooltip('enables the robot to move Backward forever ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
  }
};