Blockly.Blocks['rotateClockwise'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(210);
	this.appendDummyInput()
	.appendField('Rotate clockwise');
    this.setTooltip('enables the robot to rotate right forever ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};