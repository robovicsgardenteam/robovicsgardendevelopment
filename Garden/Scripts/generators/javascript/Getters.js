Blockly.JavaScript['getDis_IR_B'] = function(block) {
  code ='getDistanceIR()';
  return [code,Blockly.JavaScript.ORDER_ATOMIC];
};
Blockly.JavaScript['getDis_US_B'] = function(block) {
  code ='getDistanceUltraSonic()';
  return [code,Blockly.JavaScript.ORDER_ATOMIC];
  };
  
Blockly.JavaScript['getLightI_B'] = function(block) {
  code ='getLIntensity()';
  return [code,Blockly.JavaScript.ORDER_ATOMIC];
  };
  
Blockly.JavaScript['getTouch_B'] = function(block) {
  code ='getTouchLevel()';
  return [code,Blockly.JavaScript.ORDER_ATOMIC];
  };
  
Blockly.JavaScript['getNoise_B'] = function(block) {
  code ='getSoundDb()';
  return [code,Blockly.JavaScript.ORDER_ATOMIC];
  };