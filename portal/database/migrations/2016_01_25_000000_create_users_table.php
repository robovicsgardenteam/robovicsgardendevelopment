<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image_name');
            $table->timestamps();//Adds created_at and updated_at columns.
            $table->timestamp('expired_at');
            $table->integer('limit')->unsigned()->nullable();
            $table->integer('qouta')->unsigned()->nullable();

            $table->integer('type_id')->unsigned()->index();
            $table->integer('status_id')->unsigned()->index();
            $table->integer('parent_id')->unsigned()->index()->nullable();
            $table->rememberToken();

            $table->foreign('type_id')->references('id')->on('user_types');
            $table->foreign('status_id')->references('id')->on('user_status');
            $table->foreign('parent_id')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
