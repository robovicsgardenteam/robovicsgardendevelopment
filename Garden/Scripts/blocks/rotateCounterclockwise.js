Blockly.Blocks['rotateCounterclockwise'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(210);
	this.appendDummyInput()
	.appendField('Rotate counterclockwise');
    this.setTooltip('enables the robot to Rotate Left forever ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};