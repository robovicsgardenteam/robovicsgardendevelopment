Blockly.Blocks['stop'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(0);
	this.appendDummyInput()
	.appendField('Stop Robot');
    this.setTooltip('Stop The motion of the robot ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};