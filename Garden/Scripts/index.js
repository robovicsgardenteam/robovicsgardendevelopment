var code ;
var SendFlag = false;
var xml ;
var xml_text;
var xml_textl;
var w;

$( document ).ready(function() {
    loadXMLDoc();
    createWebworker();
});

function loadXMLDoc() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            loadToolBlocks(xmlhttp);
        }
    };
    xmlhttp.open("GET", "BlockPlan/bor3y.xml" , true);
    xmlhttp.send();
}

function loadToolBlocks(xml) {
    xmlDoc = xml.responseText;
    Blockly.inject(document.getElementById('blocklyDiv'), {toolbox: xmlDoc});
    Blockly.JavaScript.addReservedWords('code');
    Blockly.addChangeListener(updateCode);
}

function createWebworker(){
    if(typeof(w) == "undefined") {
        w = new Worker("webworker.js");
        w.onmessage = function(e) {
            SendFlag = false;
            cmd = getCommand(e.data);
            messageHandler(cmd);
        }
    }
}

function messageHandler(cmd){
    switch (cmd.type){
        case "connect":
            alert(cmd.message);
            ConnectionHeld();
            break;
        case "text":
            alert(cmd.message);
            break;
        case "error":
            alert("error:"+cmd.message);
            recreateWebworker();
            break;
    }
}


function getCommand(command){
    var array = command.split(";");
    var cmd = {
        type : array[0],
        message : array[1]
    };
    return cmd;
}

function refresh(){
    location.reload();
}

function ConnectionHeld(){
    document.getElementById('connect').innerHTML = "Connected";
    document.getElementById('connect').style.backgroundColor = "#00FF00";
    document.getElementById('connect').disabled = true;
}

function recreateWebworker(){
    stopWorker();
    w = new Worker("webworker.js");
    w.onmessage = function(e) {
        SendFlag = false;
        if(e.data == "connect successfully"){
            ConnectionHeld()
        }else{
            alert(e.data);
        }
    }
}

function stopWorker() {
    w.terminate();
    w = undefined;
}

function save () {
    xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    xml_text = Blockly.Xml.domToText(xml);
    saveTextAsFile();
}

function loadFileAsText()
{

    var fileToLoad = document.getElementById("fileToLoad").files[0];

    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent)
    {
        var textFromFileLoaded = fileLoadedEvent.target.result;
        xml_textl=textFromFileLoaded;
        xml1= Blockly.Xml.textToDom(xml_textl);
        Blockly.mainWorkspace.clear ();
        Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml1);
        document.getElementById("inputTextToSave").value = textFromFileLoaded;
    };
    fileReader.readAsText(fileToLoad, "UTF-8");
    $("#loadPopUp").fadeOut();

}
function newProject(){
    Blockly.mainWorkspace.clear ();
    alert('New Project Created ');

}
function saveTextAsFile()
{
    var textToWrite =xml_text;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var fileNameToSaveAs = "Program";
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    if (window.webkitURL != null)
    {
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
}

function showSignInForm()
{
    var r = confirm("Do you really want to log out?");
    if (r) {
        window.location.href = 'logoutAction.php'
    }
}

function updateCode() {

    code = Blockly.JavaScript.workspaceToCode();
}

function myUpdateFunction() {
    $("#EquivalentCode").fadeToggle();
    document.getElementById('EquivalentCode').innerHTML = code;
    document.getElementById('EquivalentCode').onblur();
}

function FadeOutCode(){
    $("#EquivalentCode").fadeOut();
}

function myExecComand() {
    Blockly.JavaScript.addReservedWords('code');
    w.postMessage(code);
}

function OpenPopUp(){
    $("#loadPopUp").fadeIn();
    $("#FileOption").fadeOut();
}


function OpenMenu()
{
    $("#FileOption").fadeToggle();
    $("#loadPopUp").fadeOut();
}

function Close()
{
    $("#loadPopUp").fadeOut();
}

function connectToEmulator()
{
    var ip = document.getElementById("IPAddress").value;
    w.postMessage(ip);
}
