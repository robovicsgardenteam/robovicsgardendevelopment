Blockly.JavaScript['moveOneCell'] = function(block) {
  // Search the text for a substring.
  var code =  'travel(20);';
  return code;
};
Blockly.JavaScript['moveOneCell_'] = function(block) {
  // Search the text for a substring.
  var code =  'travel(-20);';
  return code;
};

Blockly.JavaScript['moveCircle'] = function(block) {
  // Search the text for a substring.
var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';

	  var code =  'movArc('+ argument0+','+'360);';
  return code;
};

Blockly.JavaScript['rotate_SL'] = function(block) {
  // Search the text for a substring.

  var code =  'rotate(90);';
  return code;
};

Blockly.JavaScript['rotate_SR'] = function(block) {
  // Search the text for a substring.
  var code =  'rotate(-90);';
  return code;
};