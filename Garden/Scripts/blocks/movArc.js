Blockly.Blocks['movArc'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Move in Arc')
		.appendField(' with radius');
	this.appendValueInput('R');
	this.appendDummyInput()
		.appendField('and angle');
	this.appendValueInput('A');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to Move forward with specific radius');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};