Blockly.Blocks['travel'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Travel for distance ');
	this.appendValueInput('D');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to move forward');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};