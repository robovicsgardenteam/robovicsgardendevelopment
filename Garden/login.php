

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<html>
    <head>
        <title>roboVics Garden | empower computer science</title>
        <link rel="stylesheet" href="./css/loginStyle.css" type="text/css" media="screen" />

    <script type="text/javascript">
        function checkDateInpuWithTodays() {
             //get today's date in string
             var todayDate = new Date();
             //need to add one to get current month as it is start with 0
             var todayMonth = todayDate.getMonth() + 1;
             var todayDay = todayDate.getDate();
             var todayYear = todayDate.getFullYear();
             var todayDateText = todayDay + "/" + todayMonth + "/" + todayYear;
            alert(todayDateText);
        }
    </script>

    </head>
    <body style="background-image: url('img/WebBackground.png'); background-size:100% ; background-repeat: no-repeat;">

    <div id="Signform">
        <form id="SignINform" method="post" action="loginAction.php">

            <h3 style="">Login to Garden</h3>
            <label for="username" id="username"><b>Username</b></label>
            <br>
            <input type="text" id="login_dropdown_username" name="username" maxlength="30" class="wide username">
            <br><br>
            <label for="password" id="password"><b>Password</b></label>
            <br>
            <input type="password"  id="wide_password" name = "password">
            <br><br>
            <button type="submit" id="submit" onclick="verifyUser()"><b>Sign in</b></button> <span id="forgetpass"><a>Forget Password?</a></span>
        </form>
    </div>
    </body>
    </html>

