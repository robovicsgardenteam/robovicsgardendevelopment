<?php

use Illuminate\Database\Seeder;

class UsersTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            'name' => 'Super Admin',
        ]);

        DB::table('user_types')->insert([
            'name' => 'Editor',
        ]);

        DB::table('user_types')->insert([
            'name' => 'Super Premium',
        ]);

        DB::table('user_types')->insert([
            'name' => 'Premium',
        ]);

        DB::table('user_types')->insert([
            'name' => 'Free',
        ]);
    }
}
