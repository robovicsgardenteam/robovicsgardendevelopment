<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguagesTableSeeder::class);
        $this->call(UserStatusTableSeeder::class);
        $this->call(UsersTypesTableSeeder::class);

        $this->call(UsersTableSeeder::class);
    }
}
