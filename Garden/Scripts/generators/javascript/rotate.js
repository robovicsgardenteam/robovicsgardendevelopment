Blockly.JavaScript['rotate'] = function(block) {
  // Search the text for a substring.
var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
  var code =  'rotate('+ argument0+');';
  return code;
};