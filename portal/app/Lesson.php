<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 'lessons';

    public function examples()
    {
        return $this->hasMany('App\Example');
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }
}
