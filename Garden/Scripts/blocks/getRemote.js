Blockly.Blocks['getRemote_B'] = {
  init: function() {
  var OPERATORS =
         [['1', '1'],
         ['2','2'],
         ['3','3'],
         ['4','4']];
    this.setHelpUrl(null);
    this.setColour(250);
	
	this.appendDummyInput()
	.appendField('Get Remote Command ')
	.appendField('From Channel')
    .appendField(new Blockly.FieldDropdown(OPERATORS), 'CH');
    this.setOutput(true, 'Number');
	this.setTooltip('enables you to capture the remote button ');
  }
};