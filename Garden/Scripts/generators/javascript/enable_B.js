Blockly.JavaScript['enable_general'] = function(block) {
  var port = this.getFieldValue('po');
  var peri = this.getFieldValue('pe');
  code ='enable('+peri+','+port+');';
  return code;
};

Blockly.JavaScript['enable_pilot'] = function(block) {
  code ='enablePilot();';
  return code;
};