Blockly.Blocks['getDis_IR_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(260);
	this.appendDummyInput()
	.appendField('Get Distance Using IR Sensor');
    this.setOutput(true, 'Number');
	this.setTooltip('enables you to capture the Distance');
  }
};

Blockly.Blocks['getDis_US_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(258);
	this.appendDummyInput()
	.appendField('Get Distance Using UltraSonic Sensor');
    this.setOutput(true, 'Number');
	this.setTooltip('enables you to capture the Distance');
  }
};

Blockly.Blocks['getLightI_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(256);
	this.appendDummyInput()
	.appendField('Light Intensity');
    this.setOutput(true, 'Number');
	this.setTooltip('enables you to capture the Intensity of the light');
  }
};
Blockly.Blocks['getTouch_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(254);
	this.appendDummyInput()
	.appendField(' Is Touched');
    this.setOutput(true, 'Boolean');
	this.setTooltip('if touched return true ');
  }
};

Blockly.Blocks['getNoise_B'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(252);
	this.appendDummyInput()
	.appendField('Noise Amount');
    this.setOutput(true, 'Number');
	this.setTooltip('return amount of noise');
  }
};