<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Example extends Model
{
    protected $table = 'examples';

    public function lesson()
    {
        return $this->belongsTo('App\Lesson', 'lesson_id');
    }
}
