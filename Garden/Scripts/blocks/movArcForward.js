Blockly.Blocks['movArcForward'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(210);
	this.appendDummyInput()
		.appendField('Move in Arc Forward with radius ');
	this.appendValueInput('R');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to Move forward with specific radius');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};