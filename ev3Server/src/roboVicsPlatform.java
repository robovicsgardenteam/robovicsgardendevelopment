import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.BaseRegulatedMotor;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.sensor.NXTSoundSensor;
import lejos.hardware.sensor.NXTUltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.MeanFilter;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

class roboVicsPlatform{
	 static boolean exit = false;
	 static EV3IRSensor  IRsensor  =null;
	 static EV3ColorSensor  colorSensor  =null;
	 static EV3TouchSensor  touchSensor  =null;
	 static NXTUltrasonicSensor USSensor  =null;
	 static NXTLightSensor lSensor =null;
	 static NXTSoundSensor sSensor =null;
	 static BaseRegulatedMotor motor1  =null;
	 static BaseRegulatedMotor motor2  =null;
	 static BaseRegulatedMotor motor3  =null;
	 static BaseRegulatedMotor motor4  =null;
	 static DifferentialPilot pilot=null;
	 static SampleProvider  average ; 
	 //	flage to free Memory 			 
	 static boolean    IRsensorInitbefore1 =false ;
	 static boolean    IRsensorInitbefore2 =false ;
	 static boolean    IRsensorInitbefore3 =false ;
	 static boolean    IRsensorInitbefore4 =false ;
	 
	 static boolean    colorSensorInitbefore1 =false ;
	 static boolean    colorSensorInitbefore2 =false ;
	 static boolean    colorSensorInitbefore3 =false ;
	 static boolean    colorSensorInitbefore4 =false ;
	  
	 static boolean 	 touchSensorInitbefore1= false  ;
	 static boolean 	 touchSensorInitbefore2= false  ;
	 static boolean 	 touchSensorInitbefore3= false  ;
	 static boolean 	 touchSensorInitbefore4= false  ;

	 static boolean 	USSensorInitbefore1=false  ;
	 static boolean 	USSensorInitbefore2=false  ;
	 static boolean 	USSensorInitbefore3=false  ;
	 static boolean 	USSensorInitbefore4=false  ;
	 
	 static boolean lSensorInitbefore1 =false ;
	 static boolean lSensorInitbefore2 =false ;
	 static boolean lSensorInitbefore3 =false ;
	 static boolean lSensorInitbefore4 =false ;
	 
	 static boolean sSensorInitbefore1 =false ;
	 static boolean sSensorInitbefore2 =false ;
	 static boolean sSensorInitbefore3 =false ;
	 static boolean sSensorInitbefore4 =false ;
	 
	 static boolean motor1Initbefore =false  ;
	 static boolean motor2Initbefore =false   ;
	 static boolean motor3Initbefore =false  ;	 
	 static boolean motor4Initbefore =false  ;
	 
	 static boolean pilotInitbefore =false ;
	 static boolean averageInitbefore =false  ;

	
	public static void main(String argv[]) throws IOException     {
		// Peripherals Components .
					 			
		// Greeting Start . 
		LCD.drawString("Initialization Completed ", 1, 1);
		Sound.beep();
		
		//http server
		HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/", new httpHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
        while (true);
	}
	
	static class httpHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
        	Headers responseHeaders= t.getResponseHeaders();
        	responseHeaders.set("Access-Control-Allow-Origin", "*");
        	responseHeaders.set("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        	
        	String[] comm;
        	
        	//Connection Variables . 
    		String clientSentence;
    		String replySentence = "";		       	
        	
    		
    		//get clientSentence
            Map <String,String>parms = roboVicsPlatform.queryToMap(t.getRequestURI().getQuery());
            clientSentence = parms.get("id");
            
    		//
            try{
    			comm = clientSentence.split(" ");
    		}
    		catch(NullPointerException d){
    			LCD.drawString("error ....", 1, 6);
    			Delay.msDelay(400);
    			return;
    		}
            
			// Parsing Command . 
			comm = clientSentence.split(" ");  
			LCD.drawString(clientSentence, 1, 3);
				
			/**************************************************************************************************/			
			// Enabling Commands 
			if (comm[0].matches("0")){
				if (comm[1].matches("0")){ // color Sensor
						
					if(comm[2].matches("1")){
						if (!colorSensorInitbefore1)
								colorSensor = new EV3ColorSensor(SensorPort.S1);
							colorSensorInitbefore1 =true; 
					}
					else if(comm[2].matches("2")){
						if (!colorSensorInitbefore2)
								colorSensor = new EV3ColorSensor(SensorPort.S2);
						colorSensorInitbefore2 =true; 
					 }
					else if(comm[2].matches("3")){
						if (!colorSensorInitbefore3)
							colorSensor = new EV3ColorSensor(SensorPort.S3);
						colorSensorInitbefore3= true; 
					}
					else if(comm[2].matches("4")){
						if (!colorSensorInitbefore4)
						colorSensor = new EV3ColorSensor(SensorPort.S4);
						colorSensorInitbefore4= true; 
					 }
					replySentence+="1";	
					
					}
				
				else if (comm[1].matches("1")){ // IR Sensor  
					if(comm[2].matches("1")){
						if (!IRsensorInitbefore1) 
						IRsensor = new EV3IRSensor(SensorPort.S1);
						IRsensorInitbefore1 =true ; 
					 }
					else if(comm[2].matches("2")){
						if (!IRsensorInitbefore2) 
							IRsensor = new EV3IRSensor(SensorPort.S2);
							IRsensorInitbefore2 =true ; 
					}
					else if(comm[2].matches("3")){
						if (!IRsensorInitbefore3) 
							IRsensor = new EV3IRSensor(SensorPort.S3);
							IRsensorInitbefore3 =true ; 
					 }
					else if(comm[2].matches("4")){
						if (!IRsensorInitbefore4) 
							IRsensor = new EV3IRSensor(SensorPort.S4);
							IRsensorInitbefore4 =true ; 
						 }
					replySentence+="1";
				}
				else if (comm[1].matches("2")){ // Large Motor  
					if(comm[2].matches("1")){
						if (!motor1Initbefore)
						motor1 = new EV3LargeRegulatedMotor(MotorPort.A);
						motor1Initbefore =true ;
					}
					else if(comm[2].matches("2")){
						if (!motor2Initbefore)
							motor2 = new EV3LargeRegulatedMotor(MotorPort.B);
							motor2Initbefore =true ;
					 }
					else if(comm[2].matches("3")){
						if (!motor3Initbefore)
							motor3 = new EV3LargeRegulatedMotor(MotorPort.C);
							motor3Initbefore =true ;
					 }
					else if(comm[2].matches("4")){
						if (!motor4Initbefore)
							motor4 = new EV3LargeRegulatedMotor(MotorPort.D);
							motor4Initbefore =true ;
						 }
					replySentence+="1";
				}
				
				else if (comm[1].matches("6")){ // Medium 
					if(comm[2].matches("1")){
						if (!motor1Initbefore)
						motor1 = new EV3MediumRegulatedMotor(MotorPort.A);
						motor1Initbefore =true ;
					}
					else if(comm[2].matches("2")){
						if (!motor2Initbefore)
							motor2 = new EV3MediumRegulatedMotor(MotorPort.B);
							motor2Initbefore =true ;
					 }
					else if(comm[2].matches("3")){
						if (!motor3Initbefore)
							motor3 = new EV3MediumRegulatedMotor(MotorPort.C);
							motor3Initbefore =true ;
					 }
					else if(comm[2].matches("4")){
						if (!motor4Initbefore)
							motor4 = new EV3MediumRegulatedMotor(MotorPort.D);
							motor4Initbefore =true ;
						 }
					replySentence+="1";
				}
				else if (comm[1].matches("3")){ // ULtraSonicSensor Sensor  .
					if(comm[2].matches("1")){
						if (!USSensorInitbefore1)
						USSensor = new NXTUltrasonicSensor(SensorPort.S1);
						USSensorInitbefore1 =true ; 
					}
					else if(comm[2].matches("2")){
						if (!USSensorInitbefore2)
							USSensor = new NXTUltrasonicSensor(SensorPort.S2);
							USSensorInitbefore2 =true ; 
							}
					else if(comm[2].matches("3")){
						if (!USSensorInitbefore3)
							USSensor = new NXTUltrasonicSensor(SensorPort.S3);
							USSensorInitbefore3 =true ; 
					 }
					else if(comm[2].matches("4")){
						if (!USSensorInitbefore4)
							USSensor = new NXTUltrasonicSensor(SensorPort.S4);
							USSensorInitbefore4 =true ; 
						 }
					replySentence+="1";
				}
				
				else if (comm[1].matches("4")){ // TouchSensor .  
					if(comm[2].matches("1")){
						if (!touchSensorInitbefore1)
							touchSensor = new EV3TouchSensor(SensorPort.S1);
						touchSensorInitbefore1 =true ;
					}
					else if(comm[2].matches("2")){
						if (!touchSensorInitbefore2)
							touchSensor = new EV3TouchSensor(SensorPort.S2);
						touchSensorInitbefore2 =true ;
						 }
					else if(comm[2].matches("3")){
						if (!touchSensorInitbefore3)
							touchSensor = new EV3TouchSensor(SensorPort.S3);
						touchSensorInitbefore3 =true ;
					 }
					else if(comm[2].matches("4")){
						if (!touchSensorInitbefore4)
							touchSensor = new EV3TouchSensor(SensorPort.S4);
						touchSensorInitbefore4 =true ;
						 }
					replySentence+="1";
				}
				
				else if (comm[1].matches("5")){ // NXTLightSensor .  
					if(comm[2].matches("1")){
						if (!lSensorInitbefore1)
						lSensor = new NXTLightSensor (SensorPort.S1);
						lSensorInitbefore1=true ;
					 }
					else if(comm[2].matches("2")){
						if (!lSensorInitbefore2)
						lSensor = new NXTLightSensor (SensorPort.S2);
						lSensorInitbefore2 =true ;
					 }
					else if(comm[2].matches("3")){
						if (!lSensorInitbefore3)
						lSensor = new NXTLightSensor (SensorPort.S3);
						lSensorInitbefore3=true ;
					}
					else if(comm[2].matches("4")){
						if (!lSensorInitbefore4)
						lSensor = new NXTLightSensor (SensorPort.S4);
						lSensorInitbefore4=true;
						 }
					replySentence+="1";
				}
				else if (comm[1].matches("7")){
					if (!motor1Initbefore)
					motor1 = new EV3LargeRegulatedMotor(MotorPort.A);
					if (!motor3Initbefore)
					motor3= new EV3LargeRegulatedMotor(MotorPort.C);
					if (!pilotInitbefore)
					pilot=new DifferentialPilot(2.1f,4.5f, motor1, motor3,true);
					
					motor1Initbefore =true ; 
					motor3Initbefore =true ; 
					pilotInitbefore =true ;
					replySentence+="1";
					}
				else if (comm[1].matches("z")){
					exit = true;
					return;
					}
				else if (comm[1].matches("8")){ // NXTSoundSensor .  
					if(comm[2].matches("1")){
						if (!sSensorInitbefore1)
						sSensor = new NXTSoundSensor(SensorPort.S1);
						sSensorInitbefore1=true ;
					 }
					else if(comm[2].matches("2")){
						if (!sSensorInitbefore2)
							sSensor = new NXTSoundSensor(SensorPort.S2);
							sSensorInitbefore2=true ;
					 }
					else if(comm[2].matches("3")){
						if (!sSensorInitbefore3)
							sSensor = new NXTSoundSensor(SensorPort.S3);
							sSensorInitbefore3=true ;
					 }
					else if(comm[2].matches("4")){
						if (!sSensorInitbefore4)
							sSensor = new NXTSoundSensor(SensorPort.S4);
							sSensorInitbefore3=true ;
						 }
					replySentence+="1";
				}
			}
	
/******************************************************************************************************/			
			// Usage Commands 
						else if (clientSentence.charAt(0)=='1'){ // IRSensor (Remote). 
							int y=IRsensor.getRemoteCommand(Integer.parseInt(comm[1]));
							Delay.msDelay(150);
							if (y==IRsensor.getRemoteCommand(Integer.parseInt(comm[1]))){
							replySentence+=IRsensor.getRemoteCommand(Integer.parseInt(comm[1]));}
						}
						else if (clientSentence.charAt(0)=='2'){ // IRSensor (Distance). 
								SampleProvider s2 = IRsensor.getMode("Distance");
								float [] sample = new float [s2.sampleSize()] ;
								s2.fetchSample(sample, 0);
								int y=(int)Math.round(sample [0]);
								replySentence+=y;				
						}
						else if (clientSentence.charAt(0)=='3'){ // colorSensor (Color). 
								replySentence+= colorSensor.getColorID();
						}
						else if (clientSentence.charAt(0)=='4'){ // colorSensor (light). 
							SampleProvider s2 = colorSensor.getMode("Red");
							float [] sample = new float [s2.sampleSize()] ;
							s2.fetchSample(sample, 0);
							sample[0]*=100;
							replySentence+=(int)Math.round(sample [0]);
						}
						else if (clientSentence.charAt(0)=='5'){ // ultraSensor (Distance). 
							SampleProvider s2 = USSensor.getMode("Distance");
							 average = new MeanFilter(s2, 5);
							float [] sample = new float [average.sampleSize()] ;
							average.fetchSample(sample, 0);
							sample[0]*=100;
							replySentence+=(int)Math.round(sample [0]);
						}
						else if (clientSentence.charAt(0)=='6'){ // Light Sensor (reflection)
							SampleProvider s2 = lSensor.getAmbientMode();
							average = new MeanFilter(s2, 5);
							float [] sample = new float [average.sampleSize()]  ;
							average.fetchSample(sample, 0);
							sample[0]*=1000;
							replySentence+=(int)Math.round(sample [0]);
						}
						else if (clientSentence.charAt(0)=='7'){ // touch Sensor
							SampleProvider s2 = touchSensor.getMode(0);
							average = new MeanFilter(s2, 5);
							float [] sample = new float [average.sampleSize()]  ;
							average.fetchSample(sample, 0);
							replySentence+=(int)Math.round(sample [0]);
						}
						else if (clientSentence.charAt(0)=='8'){ // SoundSensor
							SampleProvider s2 = sSensor.getDBMode();
							average = new MeanFilter(s2, 5);
							float [] sample = new float [average.sampleSize()]  ;
							average.fetchSample(sample, 0);
							sample[0]*=100;
							replySentence+=(int)Math.round(sample [0]);
						}
						
						else if (clientSentence.charAt(0)=='9'){ //Beep
							Sound.beep();
						}
						
						else if (clientSentence.charAt(0)=='a'){ // Move motor with certain Speed 
							switch (clientSentence.charAt(2))
							{
							case 'A':
								if (comm[3].matches("F"))
								motor1.forward();
								else if (comm[3].matches("B"))
								motor1.backward();
								motor1.setSpeed(Integer.parseInt(comm[2]));
								break;
							case 'B':
								if (comm[3].matches("F"))
									motor2.forward();
									else if (comm[3].matches("B"))
									motor2.backward();
									motor2.setSpeed(Integer.parseInt(comm[2]));
									break;
							case 'C':
								if (comm[3].matches("F"))
									motor3.forward();
									else if (comm[3].matches("B"))
									motor3.backward();
									motor3.setSpeed(Integer.parseInt(comm[2]));
									break;
							case 'D':
								if (comm[3].matches("F"))
									motor3.forward();
									else if (comm[3].matches("B"))
									motor3.backward();
									motor3.setSpeed(Integer.parseInt(comm[2]));
									break;
							}
						}
						else if (clientSentence.charAt(0)=='b'){ // Move Pilot  
							switch (clientSentence.charAt(2))
							{
							case 'a':
								pilot.arc(Double.parseDouble(comm[2]), Double.parseDouble(comm[3]));
								break;
							case 'b':
								if (Integer.parseInt(comm[3])==1){
								pilot.arcForward(Double.parseDouble(comm[2]));	
								}
								else if (Integer.parseInt(comm[3])==0){
								pilot.arcBackward(Double.parseDouble(comm[2]));
								}
								break;
							case 'c':
								if (Integer.parseInt(comm[2])==1){
									pilot.forward();	
									}
								else if (Integer.parseInt(comm[2])==0){
									pilot.backward();
									}
									break;
							case 'd':
								if (Integer.parseInt(comm[2])==1){
									pilot.rotateRight();	
									}
								else if (Integer.parseInt(comm[2])==0){
									pilot.rotateLeft();
									}
									break;
							case 'e':
								pilot.travel(Double.parseDouble(comm[2]));
								break;
							
							case 'f':
								pilot.travelArc(Double.parseDouble(comm[2]), Double.parseDouble(comm[3]));
								break;
							case 'g':
								pilot.stop();
								break;
							case 'h':
								pilot.setAcceleration(Integer.parseInt(comm[2]));
								break;
							case 'i':
								pilot.setRotateSpeed(Integer.parseInt(comm[2]));
								break;
							case 'j':
								pilot.setTravelSpeed(Integer.parseInt(comm[2]));
								break;
							case 'k':
								pilot.steer(Double.parseDouble(comm[2]), Double.parseDouble(comm[3]));
								break;
							case 'l':
								pilot.rotate(Double.parseDouble(comm[2]));
								break;
							case 'm':
								replySentence+=pilot.getTravelSpeed();
								break;
							case 'n':
								replySentence+=pilot.getRotateSpeed();
								break;
							

							}
						}
						
						else if (clientSentence.charAt(0)=='c'){ // Stop Motors
							switch (clientSentence.charAt(2))
							{
							case 'A':
								if (Integer.parseInt(comm[2])== 1){
									motor1.stop();
								}
								else {
									motor1.flt();
								}
								break;
							case 'B':
								if (Integer.parseInt(comm[2])== 1){
									motor2.stop();
								}
								else {
									motor2.flt();
								}
								break;
							case 'C':
								if (Integer.parseInt(comm[2])== 1){
									motor3.stop();
								}
								else {
									motor3.flt();
								}
								break;
							case 'D':
								if (Integer.parseInt(comm[2])== 1){
									motor4.stop();
								}
								else {
									motor4.flt();
								}
								break;
							}
						}
						
						else if (clientSentence.charAt(0)=='d'){ // Draw String 
							LCD.drawString(comm[1], Integer.parseInt(comm[2]), Integer.parseInt(comm[2]));
						}
						
						else if (clientSentence.charAt(0)=='e'){ // Draw Number 
							LCD.drawInt(Integer.parseInt(comm[1]), Integer.parseInt(comm[2]), Integer.parseInt(comm[2]));
						}
						else if (clientSentence.charAt(0)=='f'){ // Clear the entire Screen . 
							LCD.clear();
						}
						else if (clientSentence.charAt(0)=='g'){ //Clear one Row . 
							LCD.clear(Integer.parseInt(comm[1]));
						}
						else if (clientSentence.charAt(0)=='h'){ //Clear one Row . 
							LCD.clear(Integer.parseInt(comm[1]),Integer.parseInt(comm[2]),Integer.parseInt(comm[3]));
						}
						else if (clientSentence.charAt(0)=='i'){ // Medium Motors
							switch (clientSentence.charAt(2))
							{
							case 'A':
								if (Integer.parseInt(comm[2])== 1){
									motor1.rotateTo(Integer.parseInt(comm[3]));
								}
								else if (Integer.parseInt(comm[2])== 2){
									motor1.rotate(Integer.parseInt(comm[3]));
								}
							
								break;
							case 'B':
								if (Integer.parseInt(comm[2])== 1){
									motor2.rotateTo(Integer.parseInt(comm[3]));
								}
								else if (Integer.parseInt(comm[2])== 2){
									motor2.rotate(Integer.parseInt(comm[3]));
								}
								break;
							case 'C':
								if (Integer.parseInt(comm[2])== 1){
									motor3.rotateTo(Integer.parseInt(comm[3]));
								}
								else if (Integer.parseInt(comm[2])== 2){
									motor3.rotate(Integer.parseInt(comm[3]));
								}
								break;
							case 'D':
								if (Integer.parseInt(comm[2])== 1){
									motor4.rotateTo(Integer.parseInt(comm[3]));
								}
								else if (Integer.parseInt(comm[2])== 2){
									motor4.rotate(Integer.parseInt(comm[3]));
								}
								break;
							}
						}

			replySentence+='\n';
			LCD.drawString(replySentence, 1, 4);
			            
            t.sendResponseHeaders(200, 			replySentence.length());
            OutputStream os = t.getResponseBody();
            os.write(			replySentence.getBytes());
            os.close();
        }
    }
    
    /**
     * returns the url parameters in a map
     * @param query
     * @return map
     */
    public static Map<String, String> queryToMap(String query){
      Map<String, String> result = new HashMap<String, String>();
      for (String param : query.split("&")) {
          String pair[] = param.split("=");
          if (pair.length>1) {
              result.put(pair[0], pair[1]);
          }else{
              result.put(pair[0], "");
          }
      }
      return result;
    }

}
