Blockly.Blocks['moveOneCell'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Move to the next Cell ');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('Move from Cell to another');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};
Blockly.Blocks['moveOneCell_'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Move to the previous Cell ');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('Move from Cell to another');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['moveCircle'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Circle')
		.appendField(' with radius');
	this.appendValueInput('R');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to Move in a circle with radius ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['rotate_SL'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Rotate 90 degree left ');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to rotate left for 90 degree ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['rotate_SR'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(120);
	this.appendDummyInput()
		.appendField('Rotate 90 degree Right ');
	 this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('enables the robot to rotate right for 90 degree ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};