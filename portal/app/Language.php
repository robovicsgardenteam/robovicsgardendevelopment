<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    protected $table = 'languages';

    public function levels()
    {
        return $this->hasMany('App\Level');
    }
}
