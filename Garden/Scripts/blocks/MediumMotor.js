Blockly.Blocks['GripOpen'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Open Grip');
    this.setTooltip('Open the Gripper Arm');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['GripClose'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Close Grip');
    this.setTooltip('Close the Gripper Arm');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['hammerUP'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('relax hammer');
    this.setTooltip('relax the hammer Arm');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};



Blockly.Blocks['hammerdown'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Knock With hammer');
    this.setTooltip('Knock With hammer');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};

Blockly.Blocks['throwBall'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Shoot ');
    this.setTooltip('throw the ball ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};
Blockly.Blocks['rotate_fan'] = {
  init: function() {
     this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Rotate fan with angle ');
	this.appendValueInput('A');
	this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('rotate fan with angle  ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};
Blockly.Blocks['rotate_fan_to'] = {
 init: function() {
    this.setHelpUrl(null);
    this.setColour(100);
	this.appendDummyInput()
	.appendField('Rotate fan To angle ');
	this.appendValueInput('A');
	this.setInputsInline(true);
	  var thisBlock = this;
    this.setTooltip('rotate fan to angle  ');
	 this.setPreviousStatement(true);
	  this.setNextStatement(true);
	  
  }
};