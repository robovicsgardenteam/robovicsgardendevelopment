<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examples', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('body');
            $table->mediumText('toolbox');
            $table->mediumText('field_matrix');
            $table->mediumText('start_workspace');
            $table->mediumText('answer_workspace');
            $table->string('thumbnail_image_name');
            $table->integer('order');
            $table->boolean('active');
            $table->timestamps();//Adds created_at and updated_at columns.
            $table->integer('lesson_id')->unsigned()->index();

            $table->foreign('lesson_id')->references('id')->on('lessons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examples');
    }
}
