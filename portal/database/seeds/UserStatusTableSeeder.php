<?php

use Illuminate\Database\Seeder;

class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_status')->insert([
            'name' => 'Active',
        ]);

        DB::table('user_status')->insert([
            'name' => 'Expired',
        ]);

        DB::table('user_status')->insert([
            'name' => 'Blocked',
        ]);
    }
}
